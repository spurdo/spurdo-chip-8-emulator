#ifndef STRUCT_H
#define STRUCT_H

#define FONT_START 0x050
#define FONT_END   0x0a0
#define VRAM_START 0Xdff
#define VRAM_END   0Xfff
#define MEM_START  0x200
#define MEM_END    0xfff

#define HEIGHT     16
#define WIDTH      32

#define INC        2      // PC incrementation

#define CLEAR()    system("@cls||clear");


struct chip_t{

  unsigned short I;
  unsigned short pc;
  unsigned char V[16];
  unsigned short A[16];
  unsigned char key[16];
  unsigned char stack_ptr;
  
  unsigned char mem[MEM_END];
};

/* CHIP INIT AND MEMORY*/
void init_chip(struct chip_t* chip);                // Initialise chip
void write_mem(struct chip_t* chip);                // Write to memory from PC
void read_mem(struct chip_t* chip);                 // Read memory starting from PC


/* VIDEO */
void draw(struct chip_t* chip);                     // Draw video memory contents to screen
void show_reg(struct chip_t* chip);                 // Show register contents


/* INSTRUCTION MANAGEMENT */
void interpreter_mode(struct chip_t* chip);         // Interpreter mode
void execution_mode(struct chip_t* chip,            // Execute all code til end from PC
		    unsigned char* c);
char default_instruction(struct chip_t* chip);      // Default instruction loop

/* TOOLS */
int read_input(unsigned char* ins);                 // Manages hexadecimal and string input. 0 -> quit, 1 -> successfully read hex input

#endif 
