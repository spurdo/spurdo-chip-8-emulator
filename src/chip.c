#include <stdio.h>
#include <stdlib.h>

#include "struct.h"


int main()
{
  struct chip_t chip;
  char current_instruction;

  init_chip(&chip);

  do{
    draw(&chip);
    current_instruction = default_instruction(&chip);
  }while(current_instruction != 'q');

  CLEAR();

  return 0;
}
