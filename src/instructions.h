#ifndef INSTRUCTIONS_H
#define INSTRUCTIONS_H

/* INSTRUCTION SET */

void cls(struct chip_t* chip);                      // 0x0000: Clear video memory
void rts(struct chip_t* chip);                      // 0x00EE: Return to subroutine
void jump(struct chip_t* chip, unsigned char* c);   // 0x1nnn: Set PC to 0xnnn
void call(struct chip_t* chip, unsigned char* c);   // 0x2nnn: Stack pointer inc, PC stacked on stack top, PC to 0xnnn
void se(struct chip_t* chip, unsigned char* c);     // 0x3xkk: Compare kk with Vx val, PC += 2 if equal
void sne(struct chip_t* chip, unsigned char* c);    // 0x4xkk: Compare kk with Vx val, PC += 2 if not equal	
void se_2(struct chip_t* chip, unsigned char* c);   // 0x5xy0: Compare Vx val with Vy val, PC += 2 if equal 	  
void store(struct chip_t* chip, unsigned char* c);  // 0x6xkk: Store value kk in register Vx
void add(struct chip_t* chip, unsigned char* c);    // 0x7xkk: Vx = Vx + kk
void ld(struct chip_t* chip, unsigned char* c);     // 0x8xy0: Vx = Vy
void or(struct chip_t* chip, unsigned char* c);     // 0x8xy1: Vx = Vx OR Vy
void and(struct chip_t* chip, unsigned char* c);    // 0x8xy2: Vx = Vx AND Vy
void xor(struct chip_t* chip, unsigned char* c);    // 0x8xy3: Vx = Vx XOR Vy
void add_2 (struct chip_t* chip, unsigned char* c); // 0x8xy4: Vx = Vx + Vy
void sub(struct chip_t* chip, unsigned char* c);    // 0x8xy5: Vx = Vx - Vy
void sne_2(struct chip_t* chip, unsigned char* c);  // 0x9xy0: Skip next instruction if Vx != Vy
void ld_2(struct chip_t* chip, unsigned char* c);   // *0xAnnn: Set I register to 0xnnn
void jp(struct chip_t* chip, unsigned char* c);     // 0xBnnn: Jump to nnn + V0
void rnd(struct chip_t* chip, unsigned char* c);    // 0xCxkk: Set Vx to random byte kk
void drw(struct chip_t* chip, unsigned char* c);    // *0xDxyn: Display n-byte sprite starting at mem location I at (Vx, Vy)
void skp(struct chip_t* chip, unsigned char* c);    // 0xEx9E: Skip next instruction if key with Vx value is pressed.
void sknp(struct chip_t* chip, unsigned char* c);   // 0xExA1: Skip next instruction if key with Vx value is NOT pressed.
void add_I(struct chip_t* chip, unsigned char* c);  // *0xFx1E: I = I + Vx

#endif
