CPPFLAGS = -MMD
CC = gcc
CFLAGS = -Wall -Wextra -std=c99
LDFLAGS =
LDLIBS =

SRC = $(wildcard src/*.c)
OBJ = ${SRC:.c=.o}
DEP = ${SRC:.c=.d}

all: prepare chip

prepare:
	@mkdir -p bin

chip: ${OBJ}
	$(CC) $(CFLAGS) $(OBJ) -o bin/chip $(LDFLAGS)

-include ${DEP}

.PHONY: clean chip

clean:
	${RM} ${OBJ}
	${RM} ${DEP}
	${RM} chip
