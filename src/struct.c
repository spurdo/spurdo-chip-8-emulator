#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "struct.h"
#include "instructions.h"

#define  VER 42

const char* help = "\nq -> Exit emulator\
              \ni -> Interpreter mode\
              \ns -> Register status\
              \nr -> Execute program starting at PC\
              \nR -> Execute program starting at PC step by step\
              \nw -> Write to memory starting at PC\
              \nj -> Set PC to 0x200\
              \n. -> Reset chip\
              \n\n(q to return)\n";

const char* instructions = "-- To jump to an address, jump 2 bytes before targeted address.\
                      \ni.e: to jump to 0x1200, jump to 0x11fe\n\
                      \n0x00E0: Clear video memory\
                      \n0x1nnn: Set PC to 0xnnn\
                      \n0x2nnn: Stack pointer inc, PC stacked on stack top, PC to 0xnnn\
                      \n0x3xkk: Compare kk with Vx val, PC += 2 if equal\
                      \n0x4xkk: Compare kk with Vx val, PC += 2 if not equal\
                      \n0x5xy0: Compare Vx val with Vy val, PC += 2 if equal\
                      \n0x6xkk: Store value kk in register Vx\
                      \n0x7xkk: Vx = Vx + kk\
                      \n0x8xy0: Vx = Vy\
                      \n0x8xy1: Vx = Vx OR Vy\
                      \n0x8xy2: Vx = Vx AND Vy\
                      \n0x8xy3: Vx = Vx XOR Vy\
                      \n0x8xy4: Vx = Vx + Vy\
                      \n0x8xy5: Vx = Vx - Vy\
                      \n0x9xy0: Skip next instruction if Vx != Vy\
                      \n0xBnnn: Jump to nnn + V0\
                      \n0xCxkk: Set Vx to random byte kk\
                      \n0xEx9E: Skip next instruction if key with Vx value is pressed.\n\n";

const unsigned char fontset[80] =
{ 
  0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
  0x20, 0x60, 0x20, 0x20, 0x70, // 1
  0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
  0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
  0x90, 0x90, 0xF0, 0x10, 0x10, // 4
  0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
  0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
  0xF0, 0x10, 0x20, 0x40, 0x40, // 7
  0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
  0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
  0xF0, 0x90, 0xF0, 0x90, 0x90, // A
  0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
  0xF0, 0x80, 0x80, 0x80, 0xF0, // C
  0xE0, 0x90, 0x90, 0x90, 0xE0, // D
  0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
  0xF0, 0x80, 0xF0, 0x80, 0x80  // F
};

/* CHIP INIT AND MEMORY */

void init_chip(struct chip_t* chip)
{
  CLEAR();
  memset(chip,0,sizeof(struct chip_t));
  
  for (int i = 0; i < FONT_END - FONT_START; i++)
    chip->mem[i + FONT_START] = fontset[i];

  chip->pc = 0x200;
}

void write_mem(struct chip_t* chip)
{
  CLEAR();
  printf("%s",instructions);
  printf("MEMORY WRITING MODE (q to quit):\n\n");

  while(chip->pc < MEM_END){

    unsigned char ins[2] = {0};
    
    printf("PC : %x\t\t",chip->pc);

    int rc = read_input(ins);
    
    if (!rc) break;
    
    chip->mem[chip->pc]   = ins[0];
    chip->mem[chip->pc+1] = ins[1];
    
    chip->pc += INC;

  }
  chip->pc -= INC;
}

void read_mem(struct chip_t* chip)
{

  CLEAR();
  printf("EXECUTING PROGRAM AT 0x%X\n\n",chip->pc);

  unsigned char ins[2];

  // Add break for invalid OPCODE
  for (;chip-> pc < VRAM_START; chip->pc += INC){

    ins[0] = chip->mem[chip->pc];
    ins[1] = chip->mem[chip->pc+1];
    
    execution_mode(chip,ins);
  }
}

void read_mem_step(struct chip_t* chip)
{

  CLEAR();
  printf("EXECUTING PROGRAM AT 0x%X\nPRESS q THEN enter TO QUIT.\n\n",chip->pc);

  unsigned char ins[2];

  // Add break for invalid OPCODE
  while (chip->pc < VRAM_START) {
    char input = getchar();
    if (input == 'q')
      break;

    ins[0] = chip->mem[chip->pc];
    ins[1] = chip->mem[chip->pc+1];

    if (!ins[0] && !ins[1])
      printf("%x === NOTHING\n", chip->pc);

    execution_mode(chip,ins);
    chip->pc += INC;
  }
}


/* VIDEO */

void draw(struct chip_t* chip)
{
  CLEAR();
  
  printf("\t\t\t\t\t\t=====SPURDO CHIP 8 version %d.%d=====\n\n\n",VER/10,VER%40);
  
  printf("\tVIDEO MEMORY:\n");
  for (unsigned short i = VRAM_START; i <= VRAM_END; i++){
    if (i % WIDTH == 0) printf("\n\t\t0x%x ---- \t  ",i);
    //if ((chip->mem[i] & 0x0f) == 0xf)
      printf("*");
      //else
      printf(" ");
  }
  
  printf("\n\n\n\n\n\nSTACK:\t\t[ ");
  for (int i = 0; i < 16; i++){
    if (i == chip->stack_ptr) printf("* ");
    i != 15 ? printf("0x%x | ",chip->A[i]) : printf("0x%x ",chip->A[i]);
  }

  printf("]\nPC:\t\t    0x%x\n\n",chip->pc);
  printf("(h for all available instructions) >> ");
}

void show_reg(struct chip_t* chip)
{
  CLEAR();
  
  for (int i = 0; i < 16; i++)
    printf("Register\tV[%x]:\t%x\n",i,chip->V[i]);

  printf("\n(q to return)\n");
  while(getchar() != 'q');
}


/* INSTRUCTION PROCESSING */

void interpreter_mode(struct chip_t* chip)
{
  CLEAR();

  while(1){

    unsigned char ins[2] = {0};
    
    printf("\nInterpreter Mode >>\t");

    int rc = read_input(ins);

    if (!rc) break;

    execution_mode(chip, ins);
  }
}

char default_instruction(struct chip_t* chip)
{  
  char ins = getchar();

  switch(ins){
    case '.': init_chip(chip); break;
    case 'i': interpreter_mode(chip); break;
    case 's': show_reg(chip); break;
    case 'r': read_mem(chip); break;
    case 'R': read_mem_step(chip); break;
    case 'w': write_mem(chip); break;
    case 'j': chip->pc = 0x200; break;
    case 'h': CLEAR(); printf("%s",help); while(getchar() != 'q'); break;
  }

  if (chip->pc > 0xfff) chip->pc = 0xf00;
  
  return ins;
}

void execution_mode(struct chip_t* chip, unsigned char ins[])
{
  unsigned char op_code_first_4bit = (ins[1] & 0xf0) >> 4;
  unsigned char op_code_last_4bit  = (ins[0] & 0x0f);
  unsigned char op_code_last_byte  = ins[0];
  //unsigned char op_code_first_byte = ins[1];
  
  unsigned short op_code = (ins[1] << 8) | ins[0];

  switch(op_code_first_4bit){

    case 0x0:
      switch(op_code){
        case 0x0001: cls(chip); break;
        case 0x00EE: rts(chip); break;
      } break;
 
    case 0x1: jump(chip, ins); break;
    case 0x2: call(chip, ins); break;
    case 0x3: se(chip, ins); break;
    case 0x4: sne(chip, ins); break;
    case 0x5: se_2(chip, ins); break;
    case 0x6: store(chip, ins); break;
    case 0x7: add(chip, ins); break;

    case 0x8:
      switch(op_code_last_4bit){
        case 0x0: ld(chip, ins); break;
        case 0x1: or(chip, ins); break;
        case 0x2: and(chip, ins); break;
        case 0x3: xor(chip, ins); break;
        case 0x4: add_2(chip, ins); break;
        case 0x5: sub(chip, ins); break;
      } break;
      
    case 0x9: sne_2(chip, ins); break;
    case 0xb: jp(chip, ins); break;
    case 0xc: rnd(chip, ins); break;

    case 0xe:
      switch(op_code_last_byte){
      case 0x9E: skp(chip, ins); break;
      case 0xA1: sknp(chip, ins); break;
      } break;
  }
}

/* TOOLS */

int read_input(unsigned char* ins)
{
  if (getchar() == 'q')
    return 0;
  
  scanf("%x",ins);
  return 1;
}
