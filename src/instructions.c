#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "struct.h"
#include "instructions.h"

void cls(struct chip_t* chip)
{
  memset(chip,0,VRAM_END);
  printf("\n=== CLS: CLEARED:\n\n=== VRAM_START:0x%x -> VRAM_END:0x%x\n",VRAM_START,VRAM_END);
}

void rts(struct chip_t* chip)
{
  if (chip->stack_ptr-1 < 0)
    printf("\n === RTS: NO PREVIOUS SUBROUTINE TO JUMP TO ===\n");
  else{
    chip->pc = chip->A[chip->stack_ptr-1];
    chip->A[chip->stack_ptr]   = 0x0;
    chip->A[chip->stack_ptr-1] = 0x0;
    chip->stack_ptr--;

    printf("\n%x === RTS: PC --> [0x%x]\n",chip->pc,chip->pc);
  }
}

void jump(struct chip_t* chip, unsigned char ins[])
{
  unsigned short nnn = ((ins[1] & 0x0f) << 8) | ins[0];

  chip->pc = nnn;
  printf("\n%x === JMP: PC --> [0x%x]\n",chip->pc,chip->pc);
}

void call(struct chip_t* chip, unsigned char ins[])
{
  if (chip->stack_ptr < 16){
    unsigned short nnn = ((ins[1] & 0x0f) << 8) | ins[0];
    unsigned short prev_pc = chip->pc;
    unsigned char prev_ptr = chip->stack_ptr;

    printf("\nLAWL\n");
    chip->A[chip->stack_ptr] = chip->pc;
    chip->stack_ptr++;
    chip->pc = nnn;
  
    printf("\n%x === CALL: A[%d] --> [0x%x]",chip->pc,prev_ptr,prev_pc);
    printf("\n%x === CALL: STACK PTR --> [%d]",chip->pc,chip->stack_ptr);
    printf("\n%x === CALL: PC --> [0x%x]\n",chip->pc,chip->pc);
  }
  else
    printf("\n === CALL: STACK POINTER REACHED LIMIT ===\n");
}

void se(struct chip_t* chip, unsigned char ins[])
{
  unsigned char kk = ins[0];
  unsigned char Vx = ins[1] & 0x0f;
  
  if (chip->V[Vx] == kk){
    chip->pc += INC;
    printf("\n%x === SE: %x = %x\n=== PC --> [0x%x]\n",chip->pc,chip->V[Vx], kk,chip->pc);
  }
}

void sne(struct chip_t* chip, unsigned char ins[])
{
  
  unsigned char kk = ins[0];
  unsigned char Vx = ins[1] & 0x0f;
  
  if (chip->V[Vx] != kk){
    chip->pc += INC;
    printf("\n%x === SNE: %x =/= %x\n=== PC --> [0x%x]\n",chip->pc,chip->V[Vx],kk,chip->pc);
  }
}

void se_2(struct chip_t* chip, unsigned char ins[])
{
        
  unsigned char Vx = ins[1] & 0x0f;
  unsigned char Vy = (ins[0] & 0xf0) >> 4;
  
  if (chip->V[Vx] == chip->V[Vy]){
    chip->pc += INC;
    printf("\n%x === SE_2: V[%x] = V[%x]\n=== PC --> [0x%x]\n",chip->pc,chip->V[Vx],chip->V[Vy],chip->pc);
  }
}

void store(struct chip_t* chip, unsigned char ins[])
{
  unsigned char kk = ins[0];
  unsigned char Vx = ins[1] & 0x0f;
  
  chip->V[Vx] = kk;
  printf("\n%x === STORE: V[%x] <-- 0x%x\n",chip->pc,Vx,kk);
}


void add(struct chip_t* chip, unsigned char ins[])
{
  unsigned char Vx = ins[1] & 0x0f;
  unsigned char kk = ins[0];

  chip->V[Vx] += kk;
  printf("\n%x === ADD: %x + %x\n",chip->pc,chip->V[Vx],kk);
}

void ld(struct chip_t* chip, unsigned char ins[])
{
  unsigned char Vx = ins[1] & 0x0f;
  unsigned char Vy = (ins[0] & 0xf0) >> 4;

  chip->V[Vx] = chip->V[Vy];
  printf("\n%x === LD: V[%x] <-- V%x\n",chip->pc, Vx,chip->V[Vy]);
}

void or(struct chip_t* chip, unsigned char ins[])
{
  unsigned char Vx = ins[1] & 0x0f;
  unsigned char Vy = (ins[0] & 0xf0) >> 4;

  chip->V[Vx] |= chip->V[Vy];
  printf("\n%x === OR: %x | %x\n",chip->pc,chip->V[Vx],chip->V[Vy]);
}

void and(struct chip_t* chip, unsigned char ins[])
{
  unsigned char Vx = ins[1] & 0x0f;
  unsigned char Vy = (ins[0] & 0xf0) >> 4;

  chip->V[Vx] &= chip->V[Vy];
  printf("\n%x === AND: %x & %x\n",chip->pc,chip->V[Vx],chip->V[Vy]);
}

void xor(struct chip_t* chip, unsigned char ins[])
{
  unsigned char Vx = ins[1] & 0x0f;
  unsigned char Vy = (ins[0] & 0xf0) >> 4;

  chip->V[Vx] ^= chip->V[Vy];
  printf("\n%x === XOR: %x ^ %x\n",chip->pc,chip->V[Vx],chip->V[Vy]);
}

void add_2(struct chip_t* chip, unsigned char ins[])
{
  unsigned char Vx = ins[1] & 0x0f;
  unsigned char Vy = (ins[0] & 0xf0) >> 4;

  chip->V[Vx] += chip->V[Vy];
  printf("\n%x === ADD_2: %x + %x\n",chip->pc,chip->V[Vx],chip->V[Vy]);
}

void sub(struct chip_t* chip, unsigned char ins[])
{
  unsigned char Vx = ins[1] & 0x0f;
  unsigned char Vy = (ins[0] & 0xf0) >> 4;

  chip->V[Vx] -= chip->V[Vy];
  printf("\n%x === SUB: %x - %x\n",chip->pc,chip->V[Vx],chip->V[Vy]);
}

void sne_2(struct chip_t* chip, unsigned char ins[])
{
  unsigned char Vx = ins[1] & 0x0f;
  unsigned char Vy = (ins[0] & 0xf0) >> 4;

  if (chip->V[Vx] != chip->V[Vy]){
    printf("\n%x === SNE_2: %x + %d",chip->pc,chip->pc,INC);
    chip->pc += INC;
    printf("\n%x === SNE_2: V[%x] != V[%x]\n",chip->pc,Vx,Vy);
  }
}

void jp(struct chip_t* chip, unsigned char ins[])
{
  unsigned short nnn = ((ins[1] & 0x0f) << 8) | ins[0];

  chip->pc = nnn + chip->V[0];
  printf("\n%x === JP: PC --> [0x%x]\n",chip->pc,chip->pc);
}


void rnd(struct chip_t* chip __attribute__((unused)), unsigned char ins[] __attribute__((unused)))
{
  /*
  srand(NULL);
  
  unsigned char val = rand();
  unsigned char kk  = ins[0];
  unsigned char Vx  = ins[1] & 0x0f;
  
  kk &= val;

  chip->V[Vx] = kk;
  printf("\n%x === RND: V[%x] = %x\n",chip->pc,Vx,kk);
  */
}

void skp(struct chip_t* chip, unsigned char ins[])
{
  unsigned char key = 0;
  unsigned char Vx  = ins[1] & 0x0f;

  scanf("%hhx", (int *)&key);
  
  if (key == chip->V[Vx]){
    chip->pc += INC;
    printf("\n%x === SKP: Pressed key: %x = V[%x]\n",chip->pc,key,Vx);
  }
}

void sknp(struct chip_t* chip, unsigned char ins[])
{
  unsigned char key = 0;
  unsigned char Vx  = ins[1] & 0x0f;

  scanf("%hhx", (int *)&key);
  
  if (key != chip->V[Vx]){
    chip->pc += INC;
    printf("\n%x === SKNP: Pressed key: %x =/= V[%x]\n",chip->pc,key,Vx);
  }
}
